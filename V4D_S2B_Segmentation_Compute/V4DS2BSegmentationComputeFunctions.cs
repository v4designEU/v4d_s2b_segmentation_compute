﻿using GH_IO.Serialization;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Data;
using Grasshopper.Kernel.Parameters;
using Grasshopper.Kernel.Special;
using Grasshopper.Kernel.Types;
using Rhino.Geometry;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace V4D_S2B_Segmentation_Compute
{
    static class V4DS2BSegmentationComputeFunctions
    {
        public static string SayHi(string name) 
        {
            return "Hello " + name + "!";
        }

        public static string RunSegmentation( string dataDirectory ) 
        {
            
            // load file from definitions directory
            var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var defdir = Path.Combine(path, "definitions");
            var defs = Directory.GetFiles(defdir, "*.gh");

 


            // read gh definition from file
            var archive = new GH_Archive();

            if (archive.ReadFromFile(defs[0]))
            {
                using (var definition = new GH_Document())
                {
                    if (!archive.ExtractObject(definition, "Definition"))
                        throw new Exception();

                    // Set input params
                    foreach (var obj in definition.Objects)
                    {
                        var group = obj as GH_Group;
                        if (group == null)
                            continue;

                        //from Resthopper
                        if (group.NickName.Contains("RH_IN"))
                        {
                            // set input

                            var param = group.Objects()[0];

                            Param_String stringParam = param as Param_String;
                            if (stringParam != null)
                            {
                                var gh_path = new GH_Path(0);
                                GH_String data = new GH_String(dataDirectory);
                                stringParam.AddVolatileData(gh_path, 0, data);        
                            }

                        }

                    }

                    // Parse output params
                    foreach (var obj in definition.Objects)
                    {
                        var group = obj as GH_Group;
                        if (group == null)
                            continue;

                        if (group.NickName.Contains("RH_OUT"))
                        {
                            // It is a RestHopper output group!
                            var param = group.Objects()[0] as IGH_Param;
                            if (param == null)
                                continue;

                            try
                            {
                                param.CollectData();
                                param.ComputeData();
                            }
                            catch (Exception)
                            {
                                param.Phase = GH_SolutionPhase.Failed;
                                // TODO: throw something better
                                throw;

                            }

                            var volatileData = param.VolatileData;
                            for (int p = 0; p < volatileData.PathCount; p++)
                            {
                                foreach (var goo in volatileData.get_Branch(p))
                                {
                                    if (goo == null)
                                        continue;

                                    if (goo.GetType() == typeof(GH_String))
                                    {
                                        GH_String ghValue = goo as GH_String;
                                        string rhValue = ghValue.Value;
                                        return rhValue;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return defs[0];
        }
    }
}
