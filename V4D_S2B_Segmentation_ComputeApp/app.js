rhino3dm().then(async m => {
    console.log('Loaded rhino3dm.');
    rhino = m; // global

    // authenticate
    RhinoCompute.authToken = RhinoCompute.getAuthToken();

    // if you have a different Rhino.Compute server, add the URL here:
    // RhinoCompute.url = "http://localhost:8888/";
    RhinoCompute.url = "http://remoteip/";

    let path = 'C:\\Users\\Administrator\\Documents\\Data\\Samples\\5.Klosterstift\\In\\';

    RhinoCompute.computeFetch('v4d/runsegmentation', [path])
        .then(function (result) {
            console.log(result);
        });
});
    