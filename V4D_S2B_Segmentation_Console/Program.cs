﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rhino.Compute;

namespace V4D_S2B_Segmentation_Console
{
    class Program
    {
        static void Main(string[] args)
        {
            ComputeServer.AuthToken = "luis@mcneel.com";
        
            ComputeServer.WebAddress = "http://localhost:8888/";
            

            // var p = @"E:\dev\v4d\V4D_S2B_Segmentation_Compute\V4D_S2B_Segmentation_Compute\bin\in\";
            var p = @"C:\Users\Administrator\Documents\Data\Samples\5.Klosterstift\In\";


         var result = Rhino.Compute.ComputeServer.Post<string>("v4d/runsegmentation", new object[] { p });
            Console.WriteLine(result);

            Console.WriteLine("press any key to exit");
            Console.ReadKey();
        }
    }
}
